<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Producto;
use App\Models\Categoria;

class CategoriaController extends Controller
{
    public function porcate($numero){
        $productos=Producto::where('categoria_id',$numero)->paginate(9);
    	return view('ordenador.categorias',['productos'=>$productos]);
    }

    public function componentes(Request $request){
    	$aux=1;
    	if (isset($request->acep)){
		   $aux=0;;
		}

        $categorias=Categoria::all();
        $productos=array();
        $repuestos=array();

        foreach ($categorias as $key => $categoria) {
            $product=Producto::where('Funcionalidad',$request->funcionalidad)->where('deFabrica',$aux)->where('categoria_id',$categoria->id)->get();

            array_push($productos, $product[0]);

            if(sizeof($product)>1){
                for($i=1;$i<sizeof($product);$i++){
                    array_push($repuestos, $product[$i]);
                }
            }
        }

    	return view('ordenador.componentes',['productos'=>$productos],['repuestos'=>$repuestos]);
    }
}
