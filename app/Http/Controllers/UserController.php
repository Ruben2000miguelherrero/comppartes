<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Venta;
use App\Models\Producto;

class UserController extends Controller
{
    public function entrar(Request $request){
        $usuario= User::where('email',$request->email)->first();
        if($usuario!=null){
            if($usuario->password==$request->password){
                session(['user' => $usuario]);
                return view('ordenador.perfil');
            }else{
                session(['aviso' => "El usuario o la contraseña no coinciden."]);
                return view('ordenador.intro');
            }
        }else{
            session(['aviso' => "El usuario o la contraseña no coinciden."]);
            return view('ordenador.intro');
        }
    }

    public function store(Request $request){
        $usuarios= User::all();
        if($request->password==$request->confirmPass){
            foreach ($usuarios as $key => $usur) {
                if($usur->email==$request->email){
                    session(['aviso' => "Este correo pertenece a otro usuario."]);
                    return redirect()->route('ordenador.registro');
                }
            }
            $datos=new User();
            $datos->nombre=$request->nombre;
            $datos->apellidos=$request->apellidos;
            $datos->email=$request->email;
            $datos->capital=0;
            $datos->password=$request->password;
            $datos->pais=$request->pais;
            $datos->provincia=$request->provincia;
            $datos->localidad=$request->localidad;
            $datos->direccion=$request->direccion;
            $datos->fechaNacimiento=$request->fechaNacimiento;
            $datos->imagen='icono.jpg';
            $datos->save();
            session(['user' => $datos]);
            return view('ordenador.perfil');
        }else{
            session(['aviso' => "Las contraseñas no coinciden."]);
            return redirect()->route('ordenador.registro');
        }
    }

    public function perfil(){
        return view('ordenador.perfil');
    }

    public function cerrar(){
        session()->forget('user');
        session()->forget('carrito');
        return view('ordenador.intro');
    }

    public function update(Request $request){
        $usuario= User::where('email',$request->correo)->first();
        $usuario->nombre=$request->nombre;
        $usuario->apellidos=$request->apellidos;
        $usuario->capital=$request->capital;
        $usuario->pais=$request->pais;
        $usuario->provincia=$request->provincia;
        $usuario->localidad=$request->localidad;
        $usuario->direccion=$request->direccion;
        $usuario->fechaNacimiento=$request->fechaNacimiento;
        if($request->password!=null && $request->password==$request->confirmPass){
            $usuario->password=$request->password;
        }
        if($request->imagen!=null){
            $entrada=$usuario->id.$request->imagen->getClientOriginalName();
            $ruta= public_path("assets/imagenes/users/");
            $request->imagen->move($ruta,$entrada);
            $usuario->imagen=$entrada;
        }
        $usuario->save();
        session()->forget('user');
        session(['user' => $usuario]);
        return view('ordenador.perfil');
    }

    public function carrito(){
        return view('ordenador.carrito');
    }

    public function confirmar(){
        $precio=0;
        foreach(session('carrito') as $clave => $unidad ){
            $datos=new Venta();
            $datos->user_id=session('user')->id;
            $datos->producto_id=$unidad['producto']->id;
            $datos->unidades=$unidad['cantidad'];
            $datos->fechaCompra=date("Y-m-j");
            $datos->save();
            //Realizamos la venta
            $produc=Producto::where('id',$unidad['producto']->id)->first();
            $apo=$produc->stock;
            $produc->stock=$apo-$unidad['cantidad'];
            $produc->save();
            //Restamos los productos comprados del stock
            $precio=$precio+($unidad['cantidad']*$unidad['producto']->precio);
        }
        $usuario= User::where('email',session('user')->email)->first();
        $aux=$usuario->capital;
        $usuario->capital=$aux-$precio;
        $usuario->save();
        //Restamos el dinero gastado de la cuenta de usuario
        session()->forget('user');
        session(['user' => $usuario]);
        session()->forget('carrito');
        session(['aviso' => "La compra se ha realizado con exito."]);
        return view('ordenador.carrito');
    }

    public function historial(){
        return view('ordenador.historial');
    }
}
