<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Producto;
use App\Models\Categoria;

class InicioController extends Controller
{
    public function home(){
    	$productos=Producto::inRandomOrder()->take(3)->get();
    	return view('ordenador.portada',['productos'=>$productos]);
    }

    public function formulario(){
    	return view('ordenador.index');
    }

    public function intro(){
        return view('ordenador.intro');
    }

    public function registro(){
        return view('ordenador.registro');
    }

}
