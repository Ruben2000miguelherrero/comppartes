<?php

namespace Database\Factories;

use App\Models\Venta;
use App\Models\User;
use App\Models\Producto;
use Illuminate\Database\Eloquent\Factories\Factory;

class VentaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Venta::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::all()->random()->id,
            'producto_id' => Producto::all()->random()->id,
            'unidades' => 1,
            'fechaCompra' => date("Y-m-j")
        ];
    }
}
