<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $usuarios=array(

    		array("nombre"=>"Ruben","apellidos"=>"Echeverria Alende","email"=>"echeverriaalende.ruben@gmail.com","capital"=>3500,"password"=>"ruben2000","pais"=>"España","provincia"=>"Cantabria","localidad"=>"Polanco","direccion"=>"Barrio Quintana Bloque 19 Bajo Derecha","fechaNacimiento"=>'2000-11-21'),

            array("nombre"=>"Marina","apellidos"=>"Ruiz Vicente","email"=>"pikachu02@hotmail.com","capital"=>4800,"password"=>"entrada2","pais"=>"España","provincia"=>"Andalucia","localidad"=>"Gibraltar","direccion"=>"Barrio Santoveña Calle 20 Numero 5","fechaNacimiento"=>'2000-01-03'),

            array("nombre"=>"Iurde","apellidos"=>"Perez Jamon","email"=>"rattata@gmail.com","capital"=>2125,"password"=>"metralleta15","pais"=>"España","provincia"=>"Galicia","localidad"=>"Pontevedra","direccion"=>"Praza do Obradoiro San Caetano 34","fechaNacimiento"=>'1998-09-13'),

            array("nombre"=>"Maria Jose","apellidos"=>"Alvarez Lopez","email"=>"marijo@gmail.com","capital"=>1200,"password"=>"homerSimp","pais"=>"España","provincia"=>"Madrid","localidad"=>"Alcalá de Henares","direccion"=>"CALLE GOYA 2, 28001 MADRID","fechaNacimiento"=>'1998-06-24'),

            array("nombre"=>"Jose Maria","apellidos"=>"Rodrigez Pedrajo","email"=>"putonberbe@hotmail.com","capital"=>200,"password"=>"sida","pais"=>"España","provincia"=>"Salamanaca","localidad"=>"Terradillos","direccion"=>"Calle Calzada Del Toro, 80, 37184","fechaNacimiento"=>'2002-02-28'),

            array("nombre"=>"Victor","apellidos"=>"Sullivan Drake","email"=>"uncharted@gmail.com","capital"=>20.520,"password"=>"viejoverde","pais"=>"España","provincia"=>"Zaragoza","localidad"=>"Monterde","direccion"=>"Plaza José Luis Arrese, 1 50213","fechaNacimiento"=>'2001-07-17')

    );

    public function run()
    {
        foreach ($this->usuarios as $usuario){
			$usuar = new User();
			$usuar->nombre = $usuario['nombre'];
            $usuar->apellidos = $usuario['apellidos'];
			$usuar->email = $usuario['email'];
            $usuar->capital = $usuario['capital'];
			$usuar->password =$usuario['password'];
            $usuar->pais = $usuario['pais'];
            $usuar->provincia = $usuario['provincia'];
            $usuar->localidad = $usuario['localidad'];
            $usuar->direccion = $usuario['direccion'];
            $usuar->fechaNacimiento = $usuario['fechaNacimiento'];
            $usuar->imagen ='icono.jpg';
			$usuar->save();
		}
		$this->command->info('Tabla usuarios inicializada con datos');
    }
}
