<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Categoria;
use Illuminate\Support\Str;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $categorias=array(

    		array("componente"=>"Placa Base","Descripcion"=>"La placa base, tal y como su propio nombre indica, sienta los cimientos sobre los que se construye un ordenador, de manera que su correcta elección es de vital importancia para la configuración de nuestra nueva máquina."),

    		array("componente"=>"Procesador","Descripcion"=>"Es el componente encargado de la ejecucion de las instrucciones de los programas. Los procesadores, también denominados CPU o micros, son un componente de vital importancia que constituye el verdadero cerebro del ordenador."),

    		array("componente"=>"Memoria RAM","Descripcion"=>"Es la memoria a corto plazo del ordenador. Su funcion es recordar la informacion que tienes en cada una de las aplicaciones abiertas en el ordenador. Sus siglas significan Random Access Memory."),

    		array("componente"=>"Disco Duro","Descripcion"=>"Un disco duro es una unidad de hardware que se usa para almacenar contenido y datos digitales en las computadoras. Todas las computadoras tienen un disco duro interno, pero también hay discos duros externos que pueden usarse para ampliar el almacenamiento de una computadora."),

    		array("componente"=>"Tarjeta Grafica","Descripcion"=>"Esta dedicada al procesamiento de datos relacionados con el video y las imagenes que se estan reproduciendo en el ordenador. Todas las imágenes que ves en el monitor de tu ordenador requieren ser procesadas por el ordenador."),

    		array("componente"=>"Fuente de alimentacion","Descripcion"=>"Convierte la corriente alterna en una forma continua de energia que los componentes necesitan para funcionar. La fuente de alimentación es una pieza crucial porque, sin ella, el resto del hardware interno no puede funcionar."),

    		array("componente"=>"Unidad Optica","Descripcion"=>"Las unidades ópticas recuperan y/o almacenan datos en discos ópticos como CDs, DVDs y discos Blu-ray, cualquiera de los cuales contiene más información.Utiliza una luz laser como parte del proceso de lectura o escritura de datos desde un archivo a discos opticos."),

    		array("componente"=>"Carcasa","Descripcion"=>"Son el armazón del equipo que contiene los componentes del computador. Su función es la de proteger los componentes del computador. Ademas, influye en la capacidad de refrigeración o la posibilidad de ampliar las prestaciones de tu configuración."),

    		array("componente"=>"Teclado","Descripcion"=>"Es un dispositivo o periférico de entrada, que utiliza un sistema de botones o teclas que envían toda la información a la computadora. No hace falta ser un prolífico novelista para sacarle rendimiento a los teclados de ordenador."),

    		array("componente"=>"Raton","Descripcion"=>"Es un dispositivo apuntador utilizado para facilitar el manejo de un entorno gráfico en una computadora. Un periférico imprescindible, no importa si necesitas un ratón inalámbrico, USB, para portátil o gaming…."),

    		array("componente"=>"Monitor","Descripcion"=>"Es el principal dispositivo de salida, que muestra datos o información a todos los usuarios. Amplia oferta en monitores para PC de primeras marcas como Asus, Samsung, BenQ o LG que garantizan una experiencia de visionado altamente satisfactoria, porque cuidar de tus ojos no es solo tarea de tu oftalmólogo.")
    );

    public function run()
    {
        foreach ($this->categorias as $categoria){
			$cat = new Categoria();
			$cat->componente = $categoria['componente'];
			$cat->Descripcion= $categoria['Descripcion'];
			$cat->save();
		}
		$this->command->info('Tabla categorias inicializada con datos');
    }
}
