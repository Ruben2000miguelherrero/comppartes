<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Venta;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
 		$this->call(UserSeeder::class);

 		DB::table('categorias')->delete();
 		$this->call(CategoriaSeeder::class);

 		DB::table('productos')->delete();
 		$this->call(ProductosSeeder::class);

 		DB::table('comentarios')->delete();
 		$this->call(ComentariosSeeder::class);

 		Venta::factory(20)->create();
    }
}
