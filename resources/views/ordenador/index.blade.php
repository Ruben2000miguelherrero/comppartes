@extends('layouts.master')
	@section('titulo')
		CompPartes
	@endsection
	
	@section('contenido')
	<body style="background-color: gray;">
		<br>
		<div class="row">
			<div class="offset-md-3 col-md-6">
				<div class="card">
				 	<div class="card-header text-center">
				 		Elegi las especificaciones
				 	</div>
				 	<div class="card-body" style="padding:30px">
				 		<form action="{{route('ordenador.componentes')}}" method="POST" enctype="multipart/form-data">
					 	@csrf
						@method('post')
					 	<div class="form-group">
					 		<label class="form-check-label" for="uso">Uso esperado</label>
					 		<div class="form-check">
							  <input class="form-check-input" type="radio" name="funcionalidad" id="funcionalidad" value="Gaming">
							  <label class="form-check-label">
							    Gaming
							  </label>
							</div>
							<div class="form-check">
							  <input class="form-check-input" type="radio" name="funcionalidad" id="funcionalidad" value="Trabajo">
							  <label class="form-check-label">
							    Trabajo
							  </label>
							</div>
							<div class="form-check">
							  <input class="form-check-input" type="radio" name="funcionalidad" id="funcionalidad" value="Uso cotidiano" required>
							  <label class="form-check-label">
							    Uso cotidiano
							  </label>
							</div>
					 	</div>
					 	<div class="form-group">
					 		<label>¿Aceptaria usar componentes de segunda mano?</label>
					 		<div class="form-check">
							  <input class="form-check-input" type="checkbox" id="acep" name="acep" value="marcado">
							  <label class="form-check-label">
							    Si, acepto.
							  </label>
							</div>
					 	</div>
					 	<div class="form-group">
					 		<label>Memoria deseada (GB):</label>
					 		<div class="form-check col-4">
							  <input type="number" id="cant" name="cant" class="form-control" min="1" value="1" required>
							</div>
					 	</div>
					 	<div class="form-group">
					 		<label>¿Cuanto esta dispuesto a gastarse?</label>
					 		<div class="form-check col-4">
							  <input type="number" id="prec" name="prec" class="form-control" min="0" value="0" required>
							</div>
					 	</div>
					 	<div class="form-group">
					 		<label class="form-check-label" for="">Sistema operativo:</label>
					 		<div class="form-check">
							  <input class="form-check-input" type="radio" name="sistem" id="sistem" value="">
							  <label class="form-check-label">
							    Microsoft Windows
							  </label>
							</div>
							<div class="form-check">
							  <input class="form-check-input" type="radio" name="sistem" id="sistem" value="">
							  <label class="form-check-label">
							    GNU/Linux
							  </label>
							</div>
							<div class="form-check">
							  <input class="form-check-input" type="radio" name="sistem" id="sistem" value="">
							  <label class="form-check-label">
							    OSX
							  </label>
							</div>
							<div class="form-check">
							  <input class="form-check-input" type="radio" name="sistem" id="sistem" value="" required>
							  <label class="form-check-label">
							    Otros ...
							  </label>
							</div>
					 	</div>
					 	<div class="form-group text-center">
					 		<button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">
					 			Aceptar
					 		</button>
					 	</div>
					 	</form>
				 	</div>
				</div>
			</div>
		</div>
		<br>
	</body>
	@endsection