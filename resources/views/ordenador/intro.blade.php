@extends('layouts.master')
	@section('titulo')
		CompPartes
	@endsection
	
	@section('contenido')
		<link href="{{asset('assets/css/centro.css')}}" rel="stylesheet">
		@if(session()->has('aviso'))
			<br>
            <ul class="list-group">
                <li class="list-group-item list-group-item-danger">{{session('aviso')}}</li>
            </ul>
            {{session()->forget('aviso')}}
        @endif
		<form action="{{route('ordenador.entrar')}}" method="POST" enctype="multipart/form-data">
			@csrf
			@method('post')
			<br>
			<table class="table table-bordered table-secondary">
				<tr>
					<td>
						<div class="container">
					    	<img id="centro" class="rounded-circle" src="{{asset('assets/imagenes/admin/captura.png')}}" style="height:150px;"/>
					  	</div>
					</td>
					<td>
						<br>
					  	<div class="form-group">
					    	<label for="email">Email:</label>
					    	<input type="email" class="form-control" name="email" id="email" placeholder="ejemplo@gmail.com">
				    	</div>
				    	<br>
				    	<div class="form-group">
					    	<label for="password">Contraseña:</label>
					    	<input type="password" class="form-control" name="password" id="password">		
					    </div>
					    <br>
					    <a href="{{route('ordenador.registro')}}">¿No esta registrado?</a>
						<button type="submit" class="btn btn-success">Entrar</button>
					</td>
				</tr>
			</table>
		</form>
	@endsection
