@extends('layouts.master')
	@section('titulo')
		CompPartes
	@endsection

	@section('contenido')
		<h1>Listado de productos</h1>
		<div class="row">
			<div>
				<ul style="list-style-type: none; z-index:-1;" class="list-group">
					<li class="list-group-item">{{$productos[0]->categoria->Descripcion}}</li>
				</ul>
			</div>
			@foreach( $productos as $clave => $producto )
				@if($producto->stock==0)
					<div class="col-xs-12 col-sm-6 col-md-4 border border-danger rounded">
				@else
					<div class="col-xs-12 col-sm-6 col-md-4 ">
				@endif
					<a href="{{route('ordenador.show',$producto)}}">
						<div class="text-center">
							<img class="rounded" src="{{asset('assets/imagenes/productos/')}}/{{$producto->imagen}}" style="height:200px;"/>
						</div>
						<h4 style="min-height:45px;margin:5px 0 10px 0" align="center">
							{{$producto->modelo}}
						</h4>
					</a>
					<ul>
						<div class="container">
							<div class="row">
								<div class="col"><li>Precio: {{$producto->precio}} euros</li></div>
								@if($producto->stock==0)
									<div class="col"><li class="text-danger">Agotado</li></div>
								@else
									@if($producto->deFabrica==1)
										<div class="col"><li>De Fabrica</li></div>
									@else
										<div class="col"><li>De segunda mano</li></div>
									@endif
								@endif
							</div>
						</div>
					</ul>
				</div>
			@endforeach
			<div class="d-flex justify-content-center">
				{{$productos->links()}}
			</div>
		</div>

	@endsection