<head>
  <style type="text/css">
      ul{
        list-style:none;/* No permite ningun estilo de lista*/
      }
      
      .nav li a { /*Etiqueta a dentro de li dentro de clase nav*/
        background-color:#212529;
        color:#fff;
        text-decoration:none;
        padding:10px 12px;
        display:block;
      }
      
      .nav li ul { /*Etiqueta ul dentro de li dentro de clase nav*/
        display:none;
        position:absolute;
        min-width:360px;
      }
      
      .nav li:hover > ul { /*Etiqueta ul hijo directo de li mientras tiene el ratone encima*/
        display:block;
      }
      
      .nav li ul table { /*Etiqueta li dentro de ul dentro de li dentro de clase nav*/
        position:relative;
      }

      .padding-0{
        padding-right:0;
        padding-left:0;
        padding-top:0;
        padding-bottom:0;
      }
    </style>
</head>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
  <a class="navbar-brand" href="{{route('home')}}">
    <img class="rounded-circle" src="{{asset('assets/imagenes/admin/captura.png')}}" style="height:30px;"/>
    CompPartes
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
      {{--@if(Auth::check() )--}}
      <li class="nav-item dropdown">
        <a class="nav-link" href="{{route('ordenador.formulario')}}">¡Crea tu ordenador!</a>
      </li>
      <li class="nav-item dropdown">
        <ul class="nav">
        <li><a class="nav-link" href="{{route('ordenador.productos')}}">Productos</a>
          <ul class='list-group'>
            <table style="z-index: 2;">
              <tr>
                <td class="padding-0"><a class="nav-link" href="{{route('ordenador.cate',1)}}">>Placa Base</a></td>
                <td class="padding-0"><a class="nav-link" href="{{route('ordenador.cate',7)}}">>Unidad Optica</a></td>
              </tr>
              <tr>
                <td class="padding-0"><a class="nav-link" href="{{route('ordenador.cate',2)}}">>Procesador</a></td>
                <td class="padding-0"><a class="nav-link" href="{{route('ordenador.cate',8)}}">>Carcasa</a></td>
              </tr>
              <tr>
                <td class="padding-0"><a class="nav-link" href="{{route('ordenador.cate',3)}}">>Memoria RAM</a></td>
                <td class="padding-0"><a class="nav-link" href="{{route('ordenador.cate',9)}}">>Teclado</a></td>
              </tr>
              <tr>
                <td class="padding-0"><a class="nav-link" href="{{route('ordenador.cate',4)}}">>Disco Duro</a></td>
                <td class="padding-0"><a class="nav-link" href="{{route('ordenador.cate',10)}}">>Raton</a></td>
              </tr>
              <tr>
                <td class="padding-0"><a class="nav-link" href="{{route('ordenador.cate',5)}}">>Tarjeta Grafica</a></td>
                <td class="padding-0"><a class="nav-link" href="{{route('ordenador.cate',11)}}">>Monitor</a></td>
              </tr>
              <tr>
                <td colspan="2" class="padding-0"><a class="nav-link" href="{{route('ordenador.cate',6)}}">>Fuente de alimentacion</a></td>
              </tr>
            </table>
          </ul>
        </li>
      </ul>
      </li>
    </ul>
    <form action="{{route('ordenador.busqueda')}}" method="POST" enctype="multipart/form-data" class="d-flex">
      @csrf
      @method('post')
        <input name="busqueda" class="form-control mr-sm-3" type="text" placeholder="Buscar" aria-label="Buscar">
    </form>
    {{--@endif --}}

    <?php
      if(session()->has('user')){

    ?>

        <ul class="navbar-nav navbar-right">
          <li class="nav-item">
            <a href="{{route('ordenador.carrito')}}"  class="nav-link">
              <?php
                echo "<img class='rounded-circle' src='assets/imagenes/admin/carro.jpg' style='height:30px'/>";
                ?>
              </a>
          </li>
          <li class="nav-item">
            <a href="{{route('ordenador.perfil')}}"  class="nav-link">
              <?php
                echo "<img class='rounded-circle' src='assets/imagenes/users/".session('user')->imagen."' style='height:30px'/>".session('user')->nombre;
                ?>
              </a>
          </li>
            <li class="nav-item">
                <a href="{{route('ordenador.cerrar')}}"  class="nav-link">
                    Cerrar
                </a>
                <form id="logout-form" action="" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
      <?php
        }else{
      ?>
        <ul class="navbar-nav navbar-right">
            <li class="nav-item">
              <a href="{{route('ordenador.intro')}}" class="nav-link">Iniciar sesion</a>
            </li>
        </ul> 
    <?php
      }
    ?>
	</div>
</div>
</nav>